package main

import (
	"fmt"
	"io/ioutil"
 	"strings"
	"sort"
	"strconv"
)

func main() {
	fmt.Println("Welcome to the 2nd day")
	dataRaw, _ := ioutil.ReadFile("./input.txt")
	var boxes = strings.Split(string(dataRaw), "\n")
	var totalPaper = 0
	var totalRibbon = 0
	for _, box := range(boxes) {
		var sizesStr = strings.Split(box, "x")
		var sizes []int
		for _, entry := range(sizesStr) {
			var value, _ = strconv.Atoi(entry)
			sizes = append(sizes, value)
		}
		sort.Ints(sizes)
		var minSquare = sizes[0] * sizes[1]
		totalRibbon += 2 * (sizes[0] + sizes[1]) + sizes[0] * sizes[1] * sizes[2]
		totalPaper += 2 * sizes[0] * sizes[1] + 2 * sizes[2] * sizes[1] + 2 * sizes[0] * sizes[2] + minSquare
	}
	fmt.Println("First step answer:", totalPaper)
	fmt.Println("Second step answer:", totalRibbon)
}