package main

import "fmt"
import "io/ioutil"

func main() {
	fmt.Println("Welcome to the 1st day")
	dataRaw, _ := ioutil.ReadFile("./input.txt")
	var data = string(dataRaw)
	var currentFloor = 0
	var atBasementFlag = false
	for pos, char := range data {
		if char == '(' {
			currentFloor++
		} else {
			currentFloor--
		}
		if currentFloor == -1 && atBasementFlag == false {
			fmt.Println("Second part answer:", pos + 1)
			atBasementFlag = true
		}
	}
	fmt.Println("First part answer:", currentFloor)
}

